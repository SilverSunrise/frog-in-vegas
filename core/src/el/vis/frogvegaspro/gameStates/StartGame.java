package el.vis.frogvegaspro.gameStates;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Array;

import el.vis.frogvegaspro.FlappyFrog;
import el.vis.frogvegaspro.gameSprites.Frog;
import el.vis.frogvegaspro.gameSprites.Barrier;

public class StartGame extends Status {
    private static final int BARRIER_SPACING = 160;
    private static final int BARRIER_COUNT = 3;

    private final Frog frog;
    private final Texture background;

    private final Array<Barrier> barriers;

    public StartGame(StatusGameManager sgm) {
        super(sgm);
        frog = new Frog(51, 250);
        camera.setToOrtho(false, FlappyFrog.WIDTH / 2, FlappyFrog.HEIGHT / 2);
        background = new Texture("background.png");


        barriers = new Array<>();

        for (int i = 0; i < BARRIER_COUNT; i++) {
            barriers.add(new Barrier(i * (BARRIER_SPACING + Barrier.BARRIER_WIDTH)));
        }

    }

    @Override
    protected void handleInput() {
        if (Gdx.input.justTouched())
            frog.jump();

    }

    @Override
    public void update(float v) {
        handleInput();
        frog.update(v);
        camera.position.x = frog.getPosition().x + 80;

        for (int i = 0; i < barriers.size; i++) {
            Barrier barrier = barriers.get(i);
            if (camera.position.x - (camera.viewportWidth / 2) > barrier.getTopBarrierPos().x + barrier.getTopBarrier().getWidth()) {
                barrier.rndPosition(barrier.getTopBarrierPos().x + ((Barrier.BARRIER_WIDTH + BARRIER_SPACING) * BARRIER_COUNT));
            }
            if (barrier.collides(frog.getBounds()))
                gsm.set(new GameOver(gsm));
        }
        camera.update();

    }

    @Override
    public void render(SpriteBatch batch) {
        batch.setProjectionMatrix(camera.combined);
        batch.begin();
        batch.draw(background, camera.position.x - (camera.viewportWidth / 2), 0);
        batch.draw(frog.getBird(), frog.getPosition().x, frog.getPosition().y);
        for (Barrier barrier : barriers) {
            batch.draw(barrier.getTopBarrier(), barrier.getBottomBarrierPos().x, barrier.getTopBarrierPos().y);
            batch.draw(barrier.getBottomBarrier(), barrier.getBottomBarrierPos().x, barrier.getBottomBarrierPos().y);
        }
        batch.end();

    }

    @Override
    public void dispose() {
        background.dispose();
        frog.dispose();
        for (Barrier barrier : barriers) {
            barrier.dispose();
        }
    }


}

