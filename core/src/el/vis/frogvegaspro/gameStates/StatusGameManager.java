package el.vis.frogvegaspro.gameStates;


import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import java.util.Stack;

public class StatusGameManager {

    private final Stack<Status> statuses;

    public StatusGameManager(){
        statuses = new Stack<>();
    }

    public void push(Status status){
        statuses.push(status);
    }

    public void set(Status status){
        statuses.pop().dispose();
        statuses.push(status);
    }

    public void update(float v){
        statuses.peek().update(v);
    }

    public void render(SpriteBatch batch){
        statuses.peek().render(batch);
    }
}
