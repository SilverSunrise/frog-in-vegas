package el.vis.frogvegaspro.gameStates;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import el.vis.frogvegaspro.FlappyFrog;

public class Menu extends Status {

    private final Texture background;
    private final Texture btnPlay;

    public Menu(StatusGameManager sgm) {
        super(sgm);
        camera.setToOrtho(false, FlappyFrog.WIDTH / 2, FlappyFrog.HEIGHT / 2);
        background = new Texture("background.png");
        btnPlay = new Texture("btn_play.png");

    }

    @Override
    public void handleInput() {
        if (Gdx.input.justTouched()) {
            gsm.set(new StartGame(gsm));
        }
    }

    @Override
    public void update(float v) {
        handleInput();
    }

    @Override
    public void render(SpriteBatch batch) {
        batch.setProjectionMatrix(camera.combined);
        batch.begin();
        batch.draw(background, 0, 0);
        batch.draw(btnPlay, camera.position.x - btnPlay.getWidth() / 2, camera.position.y);
        batch.end();
    }

    @Override
    public void dispose() {
        background.dispose();
        btnPlay.dispose();
    }
}
