package el.vis.frogvegaspro.gameStates;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector3;

public abstract class Status {
    protected OrthographicCamera camera;
    protected Vector3 mouse;
    protected StatusGameManager gsm;

    public Status(StatusGameManager sgm){
        this.gsm = sgm;
        camera = new OrthographicCamera();
        mouse = new Vector3();
    }

    protected abstract void handleInput();
    public abstract void update(float v);
    public abstract void render(SpriteBatch batch);
    public abstract void dispose();

}
