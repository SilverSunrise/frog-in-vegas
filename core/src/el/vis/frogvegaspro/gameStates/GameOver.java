package el.vis.frogvegaspro.gameStates;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import el.vis.frogvegaspro.FlappyFrog;

public class GameOver extends Status {

    private final Texture background;
    private final Texture endGame;

    public GameOver(StatusGameManager sgm) {
        super(sgm);
        camera.setToOrtho(false, FlappyFrog.WIDTH / 2, FlappyFrog.HEIGHT / 2);
        background = new Texture("background.png");
        endGame = new Texture("game_over.png");
    }

    @Override
    public void handleInput() {
        if (Gdx.input.justTouched()) {
            gsm.set(new StartGame(gsm));
        }
    }

    @Override
    public void update(float v) {
        handleInput();
    }

    @Override
    public void render(SpriteBatch batch) {
        batch.setProjectionMatrix(camera.combined);
        batch.begin();
        batch.draw(background, 0, 0);
        batch.draw(endGame, camera.position.x - endGame.getWidth() / 2, camera.position.y);
        batch.end();
    }

    @Override
    public void dispose() {
        background.dispose();
        endGame.dispose();
    }
}
