package el.vis.frogvegaspro.gameSprites;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

import java.util.Random;

public class Barrier {

    public static final int BARRIER_WIDTH = 51;

    private static final int FLUCTUATION = 135;
    private static final int BARRIER_GAP = 130;
    private static final int LOWEST_OPENING = 115;

    private Texture topBarrier, bottomBarrier;
    private Vector2 topBarrierPos, bottomBarrierPos;
    private Random rnd;
    private Rectangle topBarrierBound, bottomBarrierBound;


    public Texture getTopBarrier() {
        return topBarrier;
    }

    public Texture getBottomBarrier() {
        return bottomBarrier;
    }

    public Vector2 getTopBarrierPos() {
        return topBarrierPos;
    }

    public Vector2 getBottomBarrierPos() {
        return bottomBarrierPos;
    }

    public Barrier(float x) {
        topBarrier = new Texture("topBarrier.png");
        bottomBarrier = new Texture("topBarrier.png");
        rnd = new Random();

        topBarrierPos = new Vector2(x, rnd.nextInt(FLUCTUATION) + BARRIER_GAP + LOWEST_OPENING);
        bottomBarrierPos = new Vector2(x, topBarrierPos.y - BARRIER_GAP - bottomBarrier.getHeight());

        topBarrierBound = new Rectangle(topBarrierPos.x, topBarrierPos.y, topBarrier.getWidth(), topBarrier.getHeight());
        bottomBarrierBound = new Rectangle(bottomBarrierPos.x, bottomBarrierPos.y, bottomBarrier.getWidth(), bottomBarrier.getHeight());
    }

    public void rndPosition(float x) {
        topBarrierPos.set(x, rnd.nextInt(FLUCTUATION) + BARRIER_GAP + LOWEST_OPENING);
        bottomBarrierPos.set(x, topBarrierPos.y - BARRIER_GAP - bottomBarrier.getHeight());
        topBarrierBound.setPosition(topBarrierPos.x, topBarrierPos.y);
        bottomBarrierBound.setPosition(bottomBarrierPos.x, bottomBarrierPos.y);
    }

    public boolean collides(Rectangle player){
        return player.overlaps(topBarrierBound) || player.overlaps(bottomBarrierBound);
    }

    public void dispose() {
        topBarrier.dispose();
        bottomBarrier.dispose();
    }
}

