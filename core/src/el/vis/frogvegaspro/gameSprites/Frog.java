package el.vis.frogvegaspro.gameSprites;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;

public class Frog {
    public static final int MOVEMENT = 115;
    public static final int GRAVITY = -12;

    private Vector3 position;
    private final Vector3 speed;
    private Rectangle bounds;
    private Animation frogAnimation;

    private Texture texture;

    public Frog(int x, int y) {
        position = new Vector3(x, y, 0);
        speed = new Vector3(0, 0, 0);
        texture = new Texture("frog_animation.png");
        frogAnimation = new Animation(new TextureRegion(texture), 3,0.5f);
        bounds = new Rectangle(x, y, texture.getWidth() / 3, texture.getHeight());
    }

    public Vector3 getPosition() {
        return position;
    }

    public TextureRegion getBird() {
        return frogAnimation.getFrame();
    }

    public void update(float dt) {
        frogAnimation.update(dt);
        if (position.y > 0)
            speed.add(0, GRAVITY, 0);
        speed.scl(dt);
        position.add(MOVEMENT * dt, speed.y, 0);
        position.add(0, speed.y, 0);
        if (position.y < 0)
            position.y = 0;
        speed.scl(1 / dt);
        bounds.setPosition(position.x, position.y);
    }

    public void jump() {
        speed.y = 250;
    }

    public Rectangle getBounds() {
        return bounds;
    }

    public void dispose() {
        texture.dispose();
    }
}
