package el.vis.frogvegaspro;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import el.vis.frogvegaspro.gameStates.StatusGameManager;
import el.vis.frogvegaspro.gameStates.Menu;

public class FlappyFrog extends ApplicationAdapter {
    public static final int WIDTH = 500;
    public static final int HEIGHT = 820;

    private StatusGameManager sgm;
    private SpriteBatch batch;
    Texture texture;

    @Override
    public void create() {
        batch = new SpriteBatch();
        sgm = new StatusGameManager();
        texture = new Texture("badlogic.jpg");
        Gdx.gl.glClearColor(1, 0, 0, 1);
        sgm.push(new Menu(sgm));
    }

    @Override
    public void render() {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        sgm.update(Gdx.graphics.getDeltaTime());
        sgm.render(batch);
    }

    @Override
    public void dispose() {
        batch.dispose();
        texture.dispose();
    }
}
